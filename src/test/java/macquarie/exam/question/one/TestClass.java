package macquarie.exam.question.one;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestClass {

	@Test
	public void testSingleAddToCache() {
		TestOne one = new TestOne();
		one.addToCache(DummyData.buildCachedItem());
		assertEquals(1, one.getCache().size());
	}

	@Test
	public void testMultipleAddToCache() {
		TestOne one = new TestOne();
		for (int i = 0; i < 10; i++) {
			one.addToCache(DummyData.buildCachedItem());
		}
		assertEquals(10, one.getCache().size());
	}

	@Test
	public void testRemoveFromCache() {
		TestOne one = new TestOne();
		one.addToCache(DummyData.buildCachedItem());
		one.removeFromCache(DummyData.buildCachedItem());
		assertEquals(0, one.getCache().size());
	}

}
