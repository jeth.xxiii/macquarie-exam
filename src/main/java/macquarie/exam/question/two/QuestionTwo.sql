SELECT 
    c.name "Class Name", 
    count(distinct j.student_id) "Number of Students"
FROM
    class as c
    LEFT JOIN class_has_student j ON j.class_id = c.id
GROUP BY
    1
ORDER BY "Number of Students" desc
---------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT 
    c.name "Class Name", 
    count(distinct j.student_id) "Number of Students"
FROM
    class as c
    LEFT JOIN class_has_student j ON j.class_id = c.id
GROUP BY
    1
HAVING
	count(distinct j.student_id) >= 2
ORDER BY "Number of Students" desc