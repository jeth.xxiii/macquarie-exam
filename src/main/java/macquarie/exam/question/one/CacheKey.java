package macquarie.exam.question.one;

import java.util.Objects;

public class CacheKey {
	private final Integer intProperty;
	private final String strOne;

	public CacheKey(CachedItem item) {
		this.intProperty = item.getIntProperty();
		this.strOne = item.getStrTwo();
	}

	public Integer getIntProperty() {
		return intProperty;
	}

	public String getStrOne() {
		return strOne;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof CacheKey)) {
			return false;
		}
		CacheKey cacheKey = (CacheKey) o;
		return intProperty == cacheKey.intProperty && Objects.equals(strOne, cacheKey.strOne);
	}

	@Override
	public String toString() {
		return "CacheKey [intProperty=" + intProperty + ", strOne=" + strOne + "]";
	}

}
