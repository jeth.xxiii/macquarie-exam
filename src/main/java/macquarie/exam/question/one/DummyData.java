package macquarie.exam.question.one;

import java.util.ArrayList;
import java.util.List;

public class DummyData {

	public CacheKey buildCacheKey() {
		CacheKey cacheKey = new CacheKey(buildCachedItem());
		return cacheKey;
	}

	public static CachedItem buildCachedItem() {
		CachedItem cachedItem = new CachedItem(1, "stringOne", "stringTwo");
		return cachedItem;
	}

	public static List<CachedItem> buildMultipleCachedItem() {
		List<CachedItem> cachedItemList = new ArrayList<>();
		for (int i = 0; i < cachedItemList.size(); i++) {
			CachedItem cachedItem = new CachedItem(i, "stringOne" + i, "stringTwo" + i);
			cachedItemList.add(cachedItem);
		}
		return cachedItemList;
	}
	
	
}
