package macquarie.exam.question.one;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TestOne {

	private Map<CacheKey, List<CachedItem>> cache = new HashMap<CacheKey, List<CachedItem>>();

	public Map<CacheKey, List<CachedItem>> getCache() {
		return cache;
	}

	public void addToCache(CachedItem item) {
		CacheKey key = new CacheKey(item);

		if (!cache.containsKey(key)) {
			cache.put(key, new ArrayList<CachedItem>());
		}
		cache.get(key).add(item);

	}

	public void removeFromCache(CachedItem item) {
		CacheKey key = new CacheKey(item);

		Iterator<Map.Entry<CacheKey, List<CachedItem>>> itr = cache.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry<CacheKey, List<CachedItem>> entry = itr.next();
			if (key.equals(entry.getKey())) {
				itr.remove();
			}
		}

	}

	public void seeItemsFromCache() {
		System.out.println(cache.size());
		cache.forEach((k, v) -> {
			System.out.println("key : " + k + " value: " + v);
		});

	}

}
