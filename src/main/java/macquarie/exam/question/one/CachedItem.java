package macquarie.exam.question.one;

public class CachedItem {
	private Integer intProperty;
	private String strOne;
	private String strTwo;

	public CachedItem(Integer intProperty, String strOne, String strTwo) {
		this.intProperty = intProperty;
		this.strOne = strOne;
		this.strTwo = strTwo;
	}

	public Integer getIntProperty() {
		return intProperty;
	}

	public String getStrOne() {
		return strOne;
	}

	public String getStrTwo() {
		return strTwo;
	}

	@Override
	public String toString() {
		return "CachedItem [intProperty=" + intProperty + ", strOne=" + strOne + ", strTwo=" + strTwo + "]";
	}

}
